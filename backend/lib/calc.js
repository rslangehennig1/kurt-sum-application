const express = require('express');
const router = express.Router();

const add = function(int1, int2){
    const regexPattern = /([0-9]+)+/;
    const testComplyWithRequirements = regexPattern.test(int1);
    if (testComplyWithRequirements != true) {
        throw 'Invalid parameter'
    } else {
        return parseInt(int1) + parseInt(int2)
    }
}

router.get("/add/:int1/:int2", function(req, res){
    try{
        const result = add(req.params.int1, req.params.int2);
        return res.send({result: result});
    } catch (e) {
        res.status(500);
        return res.json({error: e})
    }
})

module.exports = { router, add };
