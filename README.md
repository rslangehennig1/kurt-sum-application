
# MagiCalc

## A totally over-engineered and very basic calculator 

The purpose of MagiCALC is to demonstrate various use cases in DevSecOps with GitLab

MagiCALC is a simple calculator application which leverages a React frontend for the user interface and a NodeJS/Express API backend to perform the calculations. These separate apps are stored as a monorepo and the GitLab CI file is configured to build the apps separately and store them as individual images within the container repo. 

